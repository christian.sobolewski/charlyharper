const mongoose = require('mongoose');
const passport = require('passport');
const router = require('express').Router();
const auth = require(`${process.env.srcDir}auth/Auth`);
const firebase = require(`${process.env.srcDir}firebase/Firebase`)
const Users = mongoose.model('User');

var HOURS_TO_NEXT_HITS = 12
var MAX_HITS = 30

//GET userMatches returns all user Matches
router.get('/userMatches', auth.required, (req, res, next) => 
{
    if (!req.query && !req.query.id) 
    {
        return res.status(400).json({error:{msg:'Request query data missing'}});
    }

    const id = req.query.id;
    Users.findById({_id: id}, function (err, currentUser) 
    {
        if (err) 
        {
            res.status(400).json({error:{msg:'User not found'}})
        }
        else 
        {
            let currentUserMatches = currentUser.matches;

            Users.find().where('_id').in(currentUserMatches).exec((err, records) => 
            {
              if (err) 
              {
                return res.status(400).json({error:{msg:err ? err.message : 'Internal server error'}});
              }
              else 
              {
                var hits = []
                
                records.forEach(user => 
                {
                  if (user.matches.indexOf(id) != -1) {
                      hits.unshift({"user":user.toProfile()})
                  }
                });
                
                res.status(200).json(hits)
              }
            });
        }
    });
});


//POST userMatche matches a user by id
router.post('/userMatch', auth.required, (req, res, next) => 
{
    if (!req.body && !req.body.user && !req.body.userMatch) 
    {
        return res.status(400).json({error:{msg:'missing'}});
    }

    const userBody = req.body.user;
    const id = userBody.id;

    const userMatchBody = req.body.userMatch;
    const userMatchId = userMatchBody.id;

    if (id == userMatchId) return res.status(400).json({error:{msg:'User could not match himself'}});

    Users.findById({_id: id}, function (err, user) 
    {
        if (!user) 
        {
          return res.status(400).json({error:{msg:'User not found'}});
        }
        else 
        {
            var update = {}
            update.accountType = user.accountType
            // if user is no more allowed to match
            if (!checkHitAllowed(user))
            {
              let hours = updateHitsTimestamp(user, res)
              if (hours < HOURS_TO_NEXT_HITS) 
              {
                return res.status(200).json({"hit":false, "hitAllowed":false,"timer":(HOURS_TO_NEXT_HITS*3600000)-(Date.now() - user.accountType.newHits)})
              } 
              else 
              {
                if (hours >= HOURS_TO_NEXT_HITS) 
                {
                  // user is allowed to match because 12 hours time limit is over
                  update.accountType.newHits = '-1'
                  update.accountType.maxHits = 1//MAX_HITS
                  update.accountType.hitsTaken = update.accountType.maxHits
                  update.accountType.newHits = Date.now()
                }
                else res.status(200).json({"hit":false, "hitAllowed":false})
              }
            }
            
            // check if id exists
            let exists = user.matches.indexOf(userMatchId);
            if (exists == -1) 
            {
                update.matches = user.matches
                update.matches.push(userMatchId)
                // if user paid and he dishits a user before remove this entry
                if (user.accountType.paid == true) {
                  let disExist = user.dismatches.indexOf(userMatchId)
                  if (disExist != -1) {
                    update.dismatches = user.dismatches
                    update.dismatches.splice(disExist, 1)
                  }
                }
                updateHitsTaken(user, update)
                Users.findByIdAndUpdate({_id:id}, update, {new: true}, function (err, updated)
                {
                  if (err) 
                  {
                    return res.status(400).json({error:{msg:err ? err.message : 'Internal server error'}});
                  } else return checkForMatch(userMatchId, id, res);
                })
            }
            else 
            {
              return checkForMatch(userMatchId, id, res);
            }
        }
    })
});

function updateHitsTimestamp(user, res) 
{
  let millisec = Date.now() - user.accountType.newHits
  let hours = (millisec / (1000 * 60 * 60)).toFixed(1);
  return hours
}

/**
 * updateHitsTaken updateds field hitsTaken on each hit only for non paid users
 * @param {*} user 
 */
function updateHitsTaken(user, update) 
{
  if (!user.accountType.paid) 
  {
    update.accountType.takenHits = user.accountType.maxHits - 1
  }
}

function checkHitAllowed(user) 
{
  if (!user.accountType.paid) 
  {
    return user.accountType.takenHits == 0 ? false : true
  } 
  else return true
}

function checkForMatch(userMatchId, id, res) 
{
  Users.findById
  (
    {_id: userMatchId}, function (err, user) 
    {
      if (err) 
      {
          return res.status(400).json({error:{msg:err ? err.message : 'Internal server error'}});
      }
      else 
      {
        if (!user) return res.status(400).json({error:{msg:'User does not exist'}});
        const hit = user.matches.indexOf(id) == -1 ? false : true
        // if we have a hit send notification to hit user
        if (hit) 
        {
          if (user.deviceToken != "") 
          {
            var payload = { "notification": {
              "title": `New Hit`,
              "body": `You have a new hit with : ${user.username}`},
              "data": {
                  "notificationType": "1",
                  "id":id
              }
            };
            var options = {priority: "high", timeToLive: 60 * 60 *24}

            firebase.messaging().sendToDevice(user.deviceToken, payload, options)
            .then(function(response) {
              console.log("Successfully sent message:", response);
            })
            .catch(function(error) {
              console.log("Error sending message:", error);
            });
          }
        }

        const json = {"hit":hit, "hitAllowed":true};
        return res.status(200).json(json);
      }
    }
  );
}

//GET userMatch profile returns profile by a given id
router.get('/matchProfile', auth.required, (req, res, next) => 
{
    if (!req.query && !req.query.userMatchId) 
    {
        return res.status(400).json({error:{msg:'Request query data missing'}});
    }

    const userMatchId = req.query.userMatchId;

    Users.findById({_id: userMatchId}, function (err, user) 
    {
      if (!user) 
      {
        return res.status(400).json({error:{msg:'User not found'}});
      }
      else 
      {
        res.status(200).json({ user: user.toProfile() })
      }
    })
});

//DEL deleteHit deletes a user hit
router.delete('/deleteHit', auth.required, (req, res, next) => 
{
    if (!req.query && !req.query.id && !req.query.userMatchId) 
    {
        return res.status(400).json({error:{msg:'Request body data missing'}});
    }

    const id = req.query.id;
    const userMatchId = req.query.userMatchId;

    Users.findById({_id: id}, function (err, user) 
    {
      if (!user || err) 
      {
        return res.status(400).json({error:{msg:'User not found'}});
      }
      else 
      {
        // check if id exists
        const exists = user.matches.indexOf(userMatchId);
        if (exists == -1 ) 
        {
          return res.status(400).json({error:{msg:'Matching does not exists'}});
        }
        else 
        {
          user.matches.splice(exists, 1);
          // find message 
          if (user.messages && user.messages.has(userMatchId))  
          {
              // delete messages
              user.messages.delete(userMatchId)
          }
          user.save(function (err) {
              if (err) 
              {
                  return res.status(400).json({error:{msg:'Internal server error'}});
              }
              else 
              {
                  res.status(200).json({"success":true})
              }
          })
        }
      }
    })
});

//DEL userMatch deletes a user match
router.delete('/deleteDismatch', auth.required, (req, res, next) => 
{
    if (!req.body && !req.body.user && !req.body.userMatch) 
    {
        return res.status(400).json({error:{msg:'missing'}});
    }

    const userBody = req.body.user;
    const id = userBody.id;

    const userMatchBody = req.body.userMatch;
    const userMatchId = userMatchBody.id;

    Users.findById({_id: id}, function (err, user) 
    {
      if (!user || err) 
      {
        return res.status(400).json({error:{msg:'User not found'}});
      }
      else 
      {
        // check if id exists
        const exists = user.dismatches.indexOf(userMatchId);
        if (exists == -1 ) 
        {
          return res.status(400).json({error:{msg:'Dismatching does not exists'}});
        }
        else 
        {
          user.dismatches.splice(exists, 1);
          user.save(function (err) {
              if (err) 
              {
                  return res.status(400).json({error:{msg:'Internal server error'}});
              }
              else 
              {
                  const json = Object.assign({}, user.dismatches);
                  res.status(200).json(json)
              }
          })
        }
      }
    })
});

//POST userMatche matches a user by id
router.post('/userDismatch', auth.required, (req, res, next) => 
{
    if (!req.body && !req.body.user && !req.body.userMatch) 
    {
        return res.status(400).json({error:{msg:'missing'}});
    }

    const userBody = req.body.user;
    const id = userBody.id;

    const userDismatchBody = req.body.userMatch;
    const userDismatchId = userDismatchBody.id;

    if (id == userDismatchId) return res.status(400).json({error:{msg:'User could not dismatch himself'}});

    Users.findById({_id: id}, function (err, user) 
    {
        if (!user || err) 
        {
          return res.status(400).json({error:{msg:'User not found'}});
        }
        else 
        {
            // check if id exists
            let exists = user.dismatches.indexOf(userDismatchId);
            if (exists == -1) 
            {
                // if user paid and he hits a user before remove this entry
                if (user.accountType.paid == true) {
                  let hitExist = user.matches.indexOf(userDismatchId)
                  if (hitExist != -1) {
                    user.matches.splice(hitExist, 1)
                  }
                }
                user.dismatches.push(userDismatchId);
                user.save(function (err) {
                    if (err) 
                    {
                        return res.status(400).json({error:{msg:err ? err.message : 'Internal server error'}});
                    }
                    else 
                    {
                      Users.findById({_id: userDismatchId}, function (err, possible) {
                        if (err) {
                          return res.status(400).json({error:{msg:err ? err.message : 'Internal server error'}});
                        }
                        else if (possible) {
                          let possibleMatch = possible.matches.indexOf(id)
                          let possibleId = possibleMatch != -1 ? userDismatchId : ""
                          res.status(200).json({"success":true, "possibleId":possibleId})
                        }
                      })
                    }
                })
            }
            else 
            {
              res.status(400).json({"error":{msg:"User dismatching already exists"}})
            }
        }
    })
});

//GET userMatches returns all user Matches
router.get('/userDisMatches', auth.required, (req, res, next) => 
{
    if (!req.query && !req.query.id) 
    {
        return res.status(400).json({error:{msg:'Request query data missing'}});
    }

    const id = req.query.id;
    Users.findById({_id: id}, function (err, currentUser) 
    {
        if (err) 
        {
            res.status(400).json({error:{msg:'User not found'}})
        }
        else 
        {
            let currentUserMatches = currentUser.dismatches;

            Users.find().where('_id').in(currentUserMatches).exec((err, records) => 
            {
              if (err) 
              {
                return res.status(400).json({error:{msg:err ? err.message : 'Internal server error'}});
              }
              else 
              {
                var hits = []
                
                records.forEach(user => 
                {
                  hits.unshift({"user":user.toProfile()})
                });
                
                res.status(200).json(hits)
              }
            });
        }
    });
});

module.exports = router;