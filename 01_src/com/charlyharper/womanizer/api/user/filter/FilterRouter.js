const mongoose = require('mongoose');
const passport = require('passport');
const router = require('express').Router();
const auth = require(`${process.env.srcDir}auth/Auth`);
const Users = mongoose.model('User');

//POST posts new filter settings for user by id
router.post('/filter', auth.required, (req, res, next) => 
{
    if (!req.body || !req.body.filter) 
    {
        return res.status(400).json({error:{msg:'Request body data missing'}});
    }

    const filterBody = req.body.filter;
    const update = {}
    update.filter = {}
    update.filter.distanceLimit = filterBody.distanceLimit
    update.filter.showGender = filterBody.showGender
    update.filter.ageRange = filterBody.ageRange

    Users.findByIdAndUpdate({_id:filterBody.id}, update, {new: true}, function(err, user) 
    {
      if (err) 
      {
        return res.status(400).json({error:{msg:'User not found'}});
      } 
      else 
      {
        if (!user) 
        {
            return res.status(400).json({error:{msg:'User not found'}});
        } 
        else 
        {
            res.status(200).json({ filter: user.filter })
        }
      }
    })
});

//GET filter returns filter by a given id
router.get('/filter', auth.required, (req, res, next) => 
{
    if (!req.query && !req.query.id) 
    {
        return res.status(400).json({error:{msg:'Request query data missing'}});
    }

    const id = req.query.id;

    Users.findById({_id: id}, function (err, user) 
    {
        if (err) 
        {
            res.status(400).json({error:{msg:'User not found'}})
        }
        else 
        {
            if (!user) 
            {
                return res.status(400).json({error:{msg:'User not found'}});
            }
            else 
            {
                if (!user.filter) 
                {
                    // return default filter
                    const defaultFilter = {"distanceLimit":20,"showGender":user.gender == 0 ? 1 : 0,"ageRange":[18,29]}
                    return res.status(200).json({filter:defaultFilter})
                }
                res.status(200).json({ filter: user.filter })
            }
        }
    })
});

module.exports = router;