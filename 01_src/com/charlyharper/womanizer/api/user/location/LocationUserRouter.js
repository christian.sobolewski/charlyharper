const mongoose = require('mongoose');
const passport = require('passport');
const router = require('express').Router();
const auth = require(`${process.env.srcDir}auth/Auth`);
const Users = mongoose.model('User');

//POST new user route (optional, everyone has access)
router.post('/locationUser', auth.required, (req, res, next) => 
{
    if (!req.body || !req.body.user) 
    {
        return res.status(400).json({error:{msg:'Request body data missing'}});
    }

    const userBody = req.body.user;
    const update = {}
    update.locality = userBody.locality
    update.loc = [userBody.longitude, userBody.latitude]

    Users.findById({_id:userBody.id}, function (err, currentUser) 
    {
      if (err) 
      {
        return res.status(400).json({error:{msg:'User not found'}});
      }
      else 
      {
        if (update.locality != currentUser.locality) 
        {

          Users.findByIdAndUpdate({_id:userBody.id}, update, {new: true}, function(err, user) 
          {
            if (err) 
            {
              return res.status(400).json({error:{msg:'User not found'}});
            } 
            else 
            {
              if (!user) 
              {
                  return res.status(400).json({error:{msg:'User not found'}});
              } 
              else 
              {
                  res.status(200).json({ "success": true })
              }
            }
          })
        }
        else res.status(200).json({ "success": true })
      }
    })
});

module.exports = router;