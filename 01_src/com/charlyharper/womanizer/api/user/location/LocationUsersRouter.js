const mongoose = require('mongoose');
const passport = require('passport');
const router = require('express').Router();
const auth = require(`${process.env.srcDir}auth/Auth`);
const distance = require('geo-distance')
const Users = mongoose.model('User');

//GET profiles to match
router.get('/userLocation', auth.required, (req, res, next) => 
{
    if (!req.query && !req.query.id) 
    {
        return res.status(400).json({error:{msg:'Request query data missing'}});
    }

    const id = req.query.id;

    Users.findById({_id: id}, function (err, currentUser) 
    {
        if (err) 
        {
            res.status(400).json({error:{msg:'User not found'}})
        }
        else 
        {
            if (!currentUser.loc) return res.status(422).json({error:{msg:'Location not found'}})
            const userMatches = currentUser.matches;
            const userDismatches = currentUser.dismatches
            const users = userMatches.concat(userDismatches)
            
            const filter = currentUser.filter ? currentUser.filter : currentUser.defaultFilter()
            const coords = [currentUser.loc[0], currentUser.loc[1]]

            Users.find({
                loc: {
                  $near: coords,
                  $maxDistance: filter.distanceLimit
                },
                gender: filter.showGender,
               _id: {"$nin":users}
              }).limit(30).exec(function(err, records) {
                if (err) {
                  return res.status(400).json({error:{msg:err ? err.message : 'Internal server error'}});
                }
                else {
                    
                    var users = []
                
                    records.forEach(user => 
                    {   
                        if (user.accountImages) 
                        {
                            if (user.age >= filter.ageRange[0] && user.age <= filter.ageRange[1]) 
                            {
                                users.push({"user":user.toProfile()})
                            }
                        }
                    });

                    res.status(200).json(users)
                }
              });
        }
    });
    
});

module.exports = router;