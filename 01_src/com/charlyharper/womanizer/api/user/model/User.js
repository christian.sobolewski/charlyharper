const mongoose = require('mongoose');

const uniqueValidator = require('mongoose-unique-validator');
const crypto = require('crypto');
const jwt = require('jsonwebtoken');

var FilterSchema = new mongoose.Schema
(
    {
      distanceLimit: {type: Number},
      showGender: {type:Number},
      ageRange: {type:Array}
    }
);


var AccountTypesSchema = new mongoose.Schema
(
    {
      paid:{type:Boolean},
      maxHits:{type:Number, default:30},
      takenHits:{type:Number}, 
      newHits:{type:String, default:'0'}
    }
);

var ProfileImageSchema = new mongoose.Schema
(
    {
      filename: {type: String, required: true},
      accountImageCreatedAt: {type:String},
      accountImageUpdatedAt: {type:String}
    }
);

var MessageSchema = new mongoose.Schema 
(
  {
    timestamp: {type:String},
    message: {type:String},
    senderId: {type:String},
    messageRead: {type:Boolean}
  },
  {
    _id: false,
    id:false,
    useNestedStrict: false
  }
)

var ConversationUserSchema = new mongoose.Schema
(
  {
    username: {type:String},
    id: {type:String},
    latestMsgTimestamp: {type:String},
    accountImageCreatedAt: {type:String},
    accountImageUpdatedAt: {type:String},
    lastLogin: {type:Date},
    conversations: {type:Array, of: MessageSchema}
  },
  {
    _id: false,
    id:false,
    useNestedStrict: false
  }
);

var UserSchema = new mongoose.Schema
(
  {
    email: {type: String, unique: true, required: [true, "E-Mail can't be blank"], match: [/\S+@\S+\.\S+/, 'is invalid'], index: true},
    password: { type: String, required: true },
    username: {type: String},
    gender: {type: Number},
    birthdate: {type:Date},
    drinkResistence: {type:Number},
    attraction: {type:Number},
    perseverance: {type:Number},
    matches:{type:Array},
    dismatches:{type:Array},
    messages:{type:Map, of: ConversationUserSchema},
    accountType:{type:Object, of: AccountTypesSchema},
    accountImages:{type:Map, of: ProfileImageSchema},
    locality:{type:String},
    loc: {type:[Number], index: "2d"},
    filter: {type:Object, of: FilterSchema},
    lastLogin: {type:Date},
    deviceToken: {type:String, default:""},
    bio: String,
    hash: String,
    salt: String
  }, 
  {
    useNestedStrict: false,
    timestamps: true
  }
);

UserSchema.plugin(uniqueValidator, {message: 'is already taken.'});

UserSchema.methods.setPassword = function(password)
{
  this.salt = crypto.randomBytes(16).toString('hex');
  this.hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
};

UserSchema.methods.validPassword = function(password) 
{
  var hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
  return this.hash === hash;
};

UserSchema.methods.initMessages = function (id, timestamp, msg, senderId, userName, accountImageCreatedAt, accountImageUpdatedAt, userUpdateAt, msgRead) 
{
  if (!this.messages) 
  {
    this.messages = new Map();
  }
  if (!this.messages.get(id)) 
  {
    this.messages.set(id, new Map())
  }
  if (!this.messages.get(id).username)
  {
    this.messages.get(id).username = userName
  }
  if (!this.messages.get(id).id)
  {
    this.messages.get(id).id = id
  }
  if (!this.messages.get(id).accountImageCreatedAt)
  {
    this.messages.get(id).accountImageCreatedAt = accountImageCreatedAt
  }
  if (!this.messages.get(id).accountImageUpdatedAt)
  {
    this.messages.get(id).accountImageUpdatedAt = accountImageUpdatedAt
  }
  if (!this.messages.get(id).latestMsgTimestamp)
  {
    this.messages.get(id).latestMsgTimestamp = timestamp
  }
  if (!this.messages.get(id).lastLogin)
  {
    this.messages.get(id).lastLogin = userUpdateAt
  }
  if (!this.messages.get(id).conversations) {
    this.messages.get(id).conversations = new Array()
  }

  if (msg.length > 0) {
    this.messages.get(id).conversations.push({timestamp:timestamp, message:msg, senderId:senderId, messageRead:msgRead})
  }
}

UserSchema.methods.generateJWT = function() 
{
  var today = new Date();
  var exp = new Date(today);
  exp.setDate(today.getDate() + 60);
  
  return jwt.sign({
    id: this._id,
    email:this.emial,
    exp: parseInt(exp.getTime() / 1000),
  }, process.env.JWT_SECRET);
};

UserSchema.methods.toProfileMessages = function (id)
{
  let messages = Array.from(this.messages.values())
  messages.sort((messageUserA, messageUserB) => {
    let tA = messageUserA ? messageUserA.timestamp : 0
    let tB =  messageUserB ? messageUserB.timestamp : 0
    if (tA > tB) { 
      return -1
    }
    if (tB > tA) {
      return 1
    }
    return 0;
  })
  const user = {
    id: this.id,
    messages:messages
  }
  return {"user":user};
}

UserSchema.methods.defaultFilter = function() 
{
  if (!this.filter) 
  {
    // return default filter
    return {"distanceLimit":20,"showGender":this.gender == 0 ? 1 : 0,"ageRange":[18,29]}
  }
}

function unreadMessages(messages) {
  let unreads = []
  if (messages) {
    let tmp = Array.from(messages.values())
    if (tmp) {
      tmp.forEach(element => {
        unreads = element.conversations.filter(element => element.messageRead != true)
      });
    }
  }
  return unreads.length
}

function calcAge(birthday) {
  var today = new Date();
  var thisYear = 0;
  // THIS must be removed after going online
  if (birthday == undefined) return 18
  if (today.getMonth() < birthday.getMonth()) {
      thisYear = 1;
  } else if ((today.getMonth() == birthday.getMonth()) && today.getDate() < birthday.getDate()) {
      thisYear = 1;
  }
  var age = today.getFullYear() - birthday.getFullYear() - thisYear;
  return age;
}

UserSchema.methods.toProfile = function ()
{
  const user = {
    id: this.id,
    username: this.username,
    gender: this.gender,
    age:calcAge(this.birthdate),
    drinkResistence: this.drinkResistence,
    attraction: this.attraction,
    perseverance: this.perseverance,
    bio: this.bio,
    locality: this.locality,
    lastLogin:this.updatedAt,
    accountImages: this.accountImages ? Array.from(this.accountImages.values()) : []
  }
  return user;
}

UserSchema.methods.toAuthJSON = function()
{
  const user = 
  {
    id:this._id,
    email:this.email,
    token:this.generateJWT(),
    username:this.username,
    gender:this.gender,
    age:calcAge(this.birthdate),
    bio:this.bio ? this.bio : "", 
    accountType: this.accountType ? this.accountType.paid ? 1 : 0 : 0,
    filter:this.filter ? this.filter : this.defaultFilter(),
    accountImageCreatedAt: this.accountImages && this.accountImages.has("0") ? this.accountImages.get("0").accountImageCreatedAt : "",
    accountImageUpdatedAt: this.accountImages && this.accountImages.has("0") ? this.accountImages.get("0").accountImageUpdatedAt : "",
    unreadMessages:unreadMessages(this.messages)
  };
  return user;
};

mongoose.model('User', UserSchema);