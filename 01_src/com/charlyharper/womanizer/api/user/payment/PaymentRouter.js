const mongoose = require('mongoose');
const passport = require('passport');
const router = require('express').Router();
const auth = require(`${process.env.srcDir}auth/Auth`);
const iap = require('@flat/in-app-purchase');
const Users = mongoose.model('User');

iap.config({
 
    /* Configurations for HTTP request */
    requestDefaults: { /* Please refer to the request module documentation here: https://www.npmjs.com/package/request#requestoptions-callback */ },
 
    /* Configurations for Amazon Store */
    //amazonAPIVersion: 2, // tells the module to use API version 2
    //secret: 'abcdefghijklmnoporstuvwxyz', // this comes from Amazon
    // amazonValidationHost: http://localhost:8080/RVSSandbox, // Local sandbox URL for testing amazon sandbox receipts.
 
    /* Configurations for Apple */
    //appleExcludeOldTransactions: true, // if you want to exclude old transaction, set this to true. Default is false
    //applePassword: 'abcdefg...', // this comes from iTunes Connect (You need this to valiate subscriptions)
 
    /* Configurations for Google Service Account validation: You can validate with just packageName, productId, and purchaseToken */
    googleServiceAccount: {
        clientEmail: process.env.GOOGLE_SERVICE_ACC_EMAIL,
        privateKey: process.env.GOOGLE_SERVICE_ACC_KEY.replace(/\\n/g, '\n')
    },
 
    /* Configurations for Roku */
    //rokuApiKey: 'aaaa...', // this comes from Roku Developer Dashboard
 
    /* Configurations all platforms */
    test: true, // For Apple and Googl Play to force Sandbox validation only
    verbose: true // Output debug logs to stdout stream
});


var ANDROID = 'android'
var IOS = 'ios'

//GET payment subscriptions
router.post('/subscriptions', auth.required, (req, res, next) => 
{
    if (!req.body && !req.body.user) 
    {
        return res.status(400).json({error:{msg:'Request query data missing'}});
    }

    const id = req.body.user.id;
    const client = req.headers["client"]

    Users.findById({_id: id}, function (err, user) 
    {
        if (err) 
        {
            res.status(400).json({error:{msg:'User not found'}})
        }
        else 
        {
            if (!user) 
            {
                return res.status(400).json({error:{msg:'User not found'}});
            }
            else 
            {
                let receipt = {
                    packageName: "com.cs.charlyharper",
                    productId: "com.cs.charlyharper.abo_a",
                    purchaseToken: "edgcacfhmkpekcilnihgdjkb.AO-J1OxnZr_-c4xGioV-wbb9YI4w7gtRzY87CRLsa6CrHuP_nF97WNzHaBjbqCyZeYYf_sZByLD1DKxkMOFlpIsiOJnSeHxu5XIwa303DbJwFQ7Lo-sM6dgY4-4DCEqk61C9qgUx0GsLaOMZJF0zMC0mRS9K8Z2P3-uSDQpUv0qorTGt7xQC42s",
                    subscription: true
                }
                iap.setup().then(() => {
                    // iap.validate(...) automatically detects what type of receipt you are trying to validate
                    iap.validate(receipt).then(onSuccess).catch(onError);
                }).catch((error) => {
                    console.log(error)
                });


                res.status(200).json({ "success": true })
            }
        }
    })
});

function onSuccess(validatedData) {
    // validatedData: the actual content of the validated receipt
    // validatedData also contains the original receipt
    var options = {
        ignoreCanceled: true, // Apple ONLY (for now...): purchaseData will NOT contain cancceled items
        ignoreExpired: true // purchaseData will NOT contain exipired subscription items
    };
    // validatedData contains sandbox: true/false for Apple and Amazon
    var purchaseData = iap.getPurchaseData(validatedData, options);
}
 
function onError(error) {
    console.log(error)
    // failed to validate the receipt...
}

module.exports = router;