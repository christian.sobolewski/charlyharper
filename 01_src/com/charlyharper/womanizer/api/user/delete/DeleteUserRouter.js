const mongoose = require('mongoose');
const passport = require('passport');
const router = require('express').Router();
const s3 = require(`${process.env.srcDir}aws-s3/MainS3`);
const auth = require(`${process.env.srcDir}auth/Auth`);
const Users = mongoose.model('User');

//DEL deleteUser delets the whole user including images
router.delete('/deleteUser', auth.required, (req, res, next) => 
{
    if (!req.query && !req.query.id) 
    {
        return res.status(400).json({error:{msg:'Request query data missing'}});
    }

    const id = req.query.id;

    Users.findById({_id: id}, function (err, user) 
    {
      if (!user) 
      {
        return res.status(400).json({error:{msg:'User not found'}});
      }
      else 
      {
        user.delete(function (err) {
            if (err) 
            {
              return res.status(400).json({error:{msg:'Internal server error'}});
            }
            else 
            {
              let params = {Bucket: `${process.env.AWS_BUCKET}`, Delete:{Objects:[{Key:`uploads/${id}`}], Quiet:false} };
              s3.deleteObjects
              (
                params, function(err, data) 
                {
                  if (err) 
                  {
                    res.status(400).json({error:{msg:'Internal server error'}});
                  }
                  else
                  {
                    res.status(200).json({success:true})
                  }
                }
              );
            }
        })
      }
    })
});

module.exports = router;