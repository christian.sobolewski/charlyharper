const mongoose = require('mongoose');
const passport = require('passport');
const router = require('express').Router();
const auth = require(`${process.env.srcDir}auth/Auth`);
const nodemailer = require('nodemailer');
const crypto = require('crypto');
const Users = mongoose.model('User');
const Token = mongoose.model('Token');

//POST new user route (optional, everyone has access)
router.post('/forgotPassword', auth.optional, (req, res, next) => 
{
    if (!req.body || !req.body.user) 
    {
        return res.status(400).json({error:{msg:'Data missing'}});
    }

    const postUser = req.body.user;

    if (!postUser.email) 
    {
        return res.status(422).json({error:{msg: 'E-Mail is required'}});
    }

    Users.exists({email:postUser.email}).then(
        function(exists)
        {
            Users.findOne({email:postUser.email}, 'id', function(err,user) {
                if (err) {
                    return res.status(400).json({error:{msg:'User not found'}});
                } else {
                    let userId = user.id
                    if (exists)
                    {
                        // Send the email
                        var transporter = nodemailer.createTransport({ host: 'smtp.gmail.com', auth: { user:"s.cloud.application.development@gmail.com", pass: "xlijpbfsvzwenbjw" } });
                        // verify connection configuration
                        transporter.verify
                        (
                            function(error, success) 
                            {
                                if (error) 
                                {
                                    return res.status(400).json({error:{msg: 'E-Mail service is down. Please try again later.'}});
                                }
                                else 
                                {
                                    let token = new Token({id: userId, token: crypto.randomBytes(16).toString('hex'), userId: userId });
                                    token.save
                                    (
                                        function (err) 
                                        {
                                            if (err) return res.status(500).json({error:{msg:err.message}});
                                            // send mail
                                            var mailOptions = { from: 's.cloud.application.development@gmail.com', to: postUser.email, subject: 'Password Forget Token', text: 'Hello,\n\n' + 'Please click the link to change your password: \n\nhttps:\/\/' + 'bambam.page.link/pw?token=' + token.token + '.\n' };
                                            transporter.sendMail(mailOptions, function (err) {
                                                if (err) { return res.status(500).send({ msg: err.message }); }
                                                res.status(200).json({ toast: 'A password forget E-Mail has been sent to ' + postUser.email + '.' })
                                            });
                                        }
                                    );
                                }
                            }
                        );
                    } 
                    else
                    {
                        return res.status(400).json({error:{msg:'User E-Mail does not exist'}})
                    }
                }
            })
        }
    );
}
);

router.post('/recoverPassword', auth.optional, (req, res, next) => 
{
    if (!req.body || !req.body.user) 
    {
        return res.status(400).json({error:{msg:'Data missing'}});
    }
    
    const authToken = req.body.user.token
    const pw = req.body.user.password

    Token.findOneAndDelete({token:authToken}, function(err, tokenUser) {
        if (err)  {
            return res.status(400).json({error:{msg:'User not found'}});
        } else if (!tokenUser) {
            return res.status(400).json({error:{msg:'Token is not longer valid. Please use password forget agains'}});
        } else {
            const id = tokenUser.userId
            Users.findById({_id: id}, function(err, user){
                if (err) {
                    return res.status(400).json({error:{msg:'User not found'}});
                } else if (!user) {
                    return res.status(400).json({error:{msg:'User not found'}});
                } else {
                    user.setPassword(pw)
                    return user.save().then(() => res.status(200).json({ user: user.toAuthJSON() })).
                    catch(err => res.status(400).json({error:{msg:'User could not be registered'}}));

                }
            })
        }
    })

})

module.exports = router;