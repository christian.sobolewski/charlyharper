const mongoose = require('mongoose');
const passport = require('passport');
const router = require('express').Router();
const auth = require(`${process.env.srcDir}auth/Auth`);
const Users = mongoose.model('User');

//POST login route (optional, everyone has access)
router.post('/loginUser', auth.optional, (req, res, next) => 
{
    if (!req.body || !req.body.user) 
    {
        return res.status(400).json({error:{msg:'Post body is incorrect'}});
    }

    const user = req.body.user;

    if (!user.email) 
    {
        return res.status(422).json({error: {msg: 'E-Mail is required'}});
    }

    if (!user.password) 
    {
        return res.status(422).json({error: {msg: 'Password is required'}});
    }
  
    return passport.authenticate('local', { session: false }, (err, passportUser, info) => 
    {
        if(err) 
        {
            return next(err);
        }
  
      if (passportUser) 
      {
        const user = passportUser;
        user.token = passportUser.generateJWT();
        user.updatedAt = Date.now()
        return user.save().then(() => res.status(200).json({ user: user.toAuthJSON() })).
              catch(err => res.status(400).json({error:{msg:'User timestamp could not be updated'}}));
      }
  
      return res.status(400).json({error:{msg:"email or password " + info.errors["email or password"]}});
    })(req, res, next);
  });

//GET current route (required, only authenticated users have access)
router.get('/autoLogin', auth.required, (req, res, next) => 
{
    if (!req.query && !req.query.id) 
    {
        return res.status(400).json({error:{msg:'Request query data missing'}});
    }

    const id = req.query.id;

    return Users.findById(id)
    .then((user) => {
      if(!user) {
        res.status(400).json({error:{msg:'User autologin not possible'}});
      }

      return user.save().then(() => res.status(200).json({ user: user.toAuthJSON() })).
              catch(err => res.status(400).json({error:{msg:'User timestamp could not be updated'}}));
    });
});

//GET dump route (required, only authenticated users have access)
router.get('/dump', auth.required, (req, res, next) => 
{
    if (!req.query && !req.query.id) 
    {
        return res.status(400).json({error:{data:'Request query data missing'}});
    }

    const id = req.query.id;

    return Users.find(id)
    .then((user) => {
      if(!user) {
        return res.sendStatus(400);
      }

      return res.json({ user: user.toAuthJSON() });
    });
});

module.exports = router;