const mongoose = require('mongoose');
const passport = require('passport');
const router = require('express').Router();
const auth = require(`${process.env.srcDir}auth/Auth`);
const Users = mongoose.model('User');

//POST this support call updates User.accountTYpe
router.post('/accountType', auth.required, (req, res, next) => 
{
    if (!req.body && !req.body.user && !req.body.user.id) 
    {
        return res.status(400).json({error:{msg:'Request body data missing'}});
    }

    const userBody = req.body.user
    const id = userBody.id
    
    const update = {accountType:{}}
    update.accountType.paid = userBody.paid
    update.accountType.maxHits = userBody.maxHits
    update.accountType.takenHits = userBody.takenHits
    update.accountType.newHits = userBody.newHits

    Users.findByIdAndUpdate({_id:id}, update, {new: true}, function(err, user) 
    {
      if (err) 
      {
        return res.status(400).json({error:{msg:'User not found'}});
      } 
      else 
      {
        if (!user) 
        {
          return res.status(400).json({error:{msg:'User not found'}});
        } 
        else 
        {
          res.status(200).json({ user: user.accountType })
        }
      }
    })
});

//POST this support call updates User.birthdate
router.post('/birthdate', auth.required, (req, res, next) => 
{
    if (!req.body && !req.body.user && !req.body.user.id) 
    {
        return res.status(400).json({error:{msg:'Request body data missing'}});
    }

    const userBody = req.body.user
    const id = userBody.id
    
    const update = {}
    update.birthdate = userBody.birthdate

    Users.findByIdAndUpdate({_id:id}, update, {new: true}, function(err, user) 
    {
      if (err) 
      {
        return res.status(400).json({error:{msg:'User not found'}});
      } 
      else 
      {
        if (!user) 
        {
          return res.status(400).json({error:{msg:'User not found'}});
        } 
        else 
        {
          res.status(200).json({ user: user.toAuthJSON() })
        }
      }
    })
});

//POST this support call removes all User.matches
router.post('/allHits', auth.required, (req, res, next) => 
{
    if (!req.body && !req.body.user && !req.body.user.id) 
    {
        return res.status(400).json({error:{msg:'Request body data missing'}});
    }

    const userBody = req.body.user
    const id = userBody.id
    
    const update = {}
    update.matches = []

    Users.findByIdAndUpdate({_id:id}, update, {new: true}, function(err, user) 
    {
      if (err) 
      {
        return res.status(400).json({error:{msg:'User not found'}});
      } 
      else 
      {
        if (!user) 
        {
          return res.status(400).json({error:{msg:'User not found'}});
        } 
        else 
        {
          res.status(200).json({ userMatches: user.matches })
        }
      }
    })
});

//POST this support call removes all User.dismatches
router.post('/allDisHits', auth.required, (req, res, next) => 
{
    if (!req.body && !req.body.user && !req.body.user.id) 
    {
        return res.status(400).json({error:{msg:'Request body data missing'}});
    }

    const userBody = req.body.user
    const id = userBody.id
    
    const update = {}
    update.dismatches = []

    Users.findByIdAndUpdate({_id:id}, update, {new: true}, function(err, user) 
    {
      if (err) 
      {
        return res.status(400).json({error:{msg:'User not found'}});
      } 
      else 
      {
        if (!user) 
        {
          return res.status(400).json({error:{msg:'User not found'}});
        } 
        else 
        {
          res.status(200).json({ userDismatches: user.dismatches })
        }
      }
    })
});

module.exports = router;