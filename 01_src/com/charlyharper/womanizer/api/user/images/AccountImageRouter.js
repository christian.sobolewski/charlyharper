const fs  = require('fs');
const path = require('path')
const mongoose = require('mongoose');
const passport = require('passport');
const s3 = require(`${process.env.srcDir}aws-s3/MainS3`);
const router = require('express').Router();
const auth = require(`${process.env.srcDir}auth/Auth`);
const Users = mongoose.model('User');

//POST profile image route 
router.post('/uploadAccountImage', auth.required, (req, res, next) => 
{
    if (!req.files || Object.keys(req.files).length === 0 || !req.query || !req.query.id || !req.query.slotId) 
    {
        return res.status(400).json({error:{msg:'Uploading image could not be finalized'}});
    }
    else
    {
        const id = req.query.id;
        const slotId = req.query.slotId;
        let accountImage = req.files.accountImage;

        if (slotId > 4) return res.status(400).json({error:{msg:`Uploading image for slot: ${slotId} not allowed`}});

        Users.findById({_id: id}, function (err, user) 
        {
          if (!user) 
          {
            return res.status(400).json({error:{msg:'User not found'}});
          }
          else 
          {
            if (!user.accountImages) 
            {
              var image = {filename:accountImage.name, accountImageCreatedAt: Date.now().toString(), accountImageUpdatedAt: Date.now().toString()}
              user.accountImages = new Map();
              user.accountImages.set(slotId, image)
            } 
            else 
            {
              var image = user.accountImages.get(slotId)
              if (!image) 
              {
                image = {filename:accountImage.name, accountImageCreatedAt: Date.now().toString(), accountImageUpdatedAt: Date.now().toString()}
              } 
              else 
              { 
                image.filename = accountImage.name
                image.accountImageCreatedAt = image.accountImageUpdatedAt ? image.accountImageUpdatedAt : Date.now().toString()
                image.accountImageUpdatedAt = Date.now().toString()
              }
              user.accountImages.set(slotId, image)
            }
            
            let params = {Bucket: `${process.env.AWS_BUCKET}/uploads/${id}/images`, Key: `${id}_${slotId}`, Body: accountImage.data};
            s3.upload
            (
              params, (err, data) => 
              {
                if (err) 
                {
                  return res.status(400).json({error:{msg:`Upload Error ${err}`}});
                }
                else 
                {
                  
                  user.save
                  (
                    function (err) 
                    {
                      if (err) 
                      {
                        return res.status(400).json({error:{msg:'Internal server error'}});
                      }
                      else 
                      {
                        res.status(200).json({ "success":true,id:id,slotId:slotId,accountImageCreatedAt:image.accountImageCreatedAt,accountImageUpdatedAt:image.accountImageUpdatedAt})
                      }
                    }
                  );
                }
              }
            );

          }
        })
    }
});

// get profile image
router.get('/accountImage', auth.required, (req, res, next) => 
{
    if (!req.query && !req.query.id || !req.query.slotId) 
    {
        return res.status(400).json({error:{msg:'Request query data missing'}});
    }

    const id = req.query.id;
    const slotId = req.query.slotId;
    if (slotId > 4) return res.status(400).json({error:{msg:`Downloading image for slot: ${slotId} not allowed`}});

    Users.findById
    (
        {_id: id}, (err, user) => 
        {
            if (err) 
            {
              res.status(400).json({error:{msg:err.message}});
            }
            else 
            {
              if (!user) return res.status(400).json({error:{msg:'User not found'}});


              if (user.accountImages && user.accountImages.get(slotId)) 
              {
                let params = {Bucket: `${process.env.AWS_BUCKET}/uploads/${id}/images`, Key: `${id}_${slotId}`};
                s3.getObject(params)
                .on('httpHeaders', function (statusCode, headers) {
                    res.set('Content-Length', headers['content-length']);
                    res.set('Content-Type', headers['content-type']);
                    res.set("accountImageCreatedAt", user.accountImages.get(slotId).accountImageCreatedAt)
                    res.set("accountImageUpdatedAt", user.accountImages.get(slotId).accountImageUpdatedAt)
                    this.response.httpResponse.createUnbufferedStream()
                        .pipe(res);
                })
                .send();
              }
              else {
                res.status(400).json({error:{msg:`Downloading image for slot: ${slotId} not found`}});
              }
            }
      })
});

//DEL userMatch deletes a user match
router.delete('/deleteAccountImage', auth.required, (req, res, next) => 
{
  if (!req.query && !req.query.id || !req.query.slotId) 
  {
      return res.status(400).json({error:{msg:'Request query data missing'}});
  }

  const id = req.query.id
  const slotId = req.query.slotId
  if (slotId > 4) return res.status(400).json({error:{msg:`Deleting image for slot: ${slotId} not allowed`}});

  Users.findById
  (
      {_id: id}, (err, user) => 
      {
          if (err) 
          {
            res.status(400).json({error:{msg:err.message}});
          }
          else 
          {
            if (!user) return res.status(400).json({error:{msg:'User not found'}});


            if (user.accountImages && user.accountImages.get(slotId)) 
            {
              user.accountImages.delete(slotId)
              user.save(function (err) {
                  if (err) 
                  {
                      return res.status(400).json({error:{msg:'Internal server error'}});
                  }
                  else 
                  {
                    let params = {Bucket: `${process.env.AWS_BUCKET}/uploads/${id}/images`, Key: `${id}_${slotId}`};
                    s3.deleteObject(params, function(err, data) {
                      if (err) 
                      {
                        console.log(err, err.stack);
                      } 
                      else 
                      { 
                        res.status(200).json({"success":true})
                      }          
                    });
                  }
              })
            }
            else res.status(400).json({error:{msg:`Image to delete for slot: ${slotId} not found`}});
          }
    })

})

module.exports = router;