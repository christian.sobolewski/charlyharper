const mongoose = require('mongoose');
const passport = require('passport');
const router = require('express').Router();
const auth = require(`${process.env.srcDir}auth/Auth`);
const Users = mongoose.model('User');

var MAX_HITS = 30

//POST new user route (optional, everyone has access)
router.post('/registerUser', auth.optional, (req, res, next) => 
{
    if (!req.body || !req.body.user) 
    {
        return res.status(400).json({error:{msg:'Data missing'}});
    }

    const user = req.body.user;

    if (!user.email) {
        return res.status(422).json({error:{msg: 'E-Mail is required'}});
    }

    if (!user.password) {
        return res.status(422).json({error:{msg:'Password is required'}});
    }

    if (!user.username) {
        return res.status(422).json({error:{msg:'Username is required'}});
    }
    
    if (parseInt(user.gender) == 0 || parseInt(user.gender) == 1) {
    } else return res.status(422).json({error:{msg:'Gender is required'}});

    
    if (!user.birthdate) {
        return res.status(422).json({error:{msg:'Age is required'}});
    }

    Users.exists({email:user.email}).then(
        function(exists)
        {
            if (exists)
            {
                return res.status(400).json({error:{msg:'User already exits'}})
            } 
            else
            {

                const finalUser = new Users(user)
                finalUser.setPassword(user.password)
                finalUser.username = user.username
                finalUser.gender = user.gender
                finalUser.birthdate = user.birthdate
                finalUser.messages = new Map() 
                finalUser.accountType = {paid:false, maxHits:MAX_HITS, hitsTaken:0, newHits:'0'}
                
                return finalUser.save().then(() => res.status(200).json({ user: finalUser.toAuthJSON() })).
                catch(err => res.status(400).json({error:{msg:'User could not be registered'}}));
            }
        }
    );
}
);

module.exports = router;