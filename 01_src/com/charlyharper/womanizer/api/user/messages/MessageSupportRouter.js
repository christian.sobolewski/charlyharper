const mongoose = require('mongoose');
const passport = require('passport');
const router = require('express').Router();
const auth = require(`${process.env.srcDir}auth/Auth`);
const Users = mongoose.model('User');

//DELETE report route (required, only authenticated users have access)
router.delete('/report', auth.required, (req, res, next) => 
{
    if (!req.query && !req.query.id && !req.query.userMatchId) 
    {
        return res.status(400).json({error:{msg:'Request body data missing'}});
    }

    const id = req.query.id;
    const senderId = req.query.userMatchId

    Users.findById({_id: id}, function (err, user) 
    {
        if (!user) 
        {
          return res.status(400).json({error:{msg:'User not found'}});
        } 
        else 
        {
            // find hit in machtes
            let hitIdx = user.matches.indexOf(senderId)
            if (hitIdx != -1) 
            {
                // remove hit from matches
                user.matches.splice(hitIdx, 1)
                // find dishit in dismatches
                let disIdx = user.dismatches.indexOf(senderId)
                if (disIdx != -1) 
                {
                    // add dismatch
                    user.dismatches.push(senderId)
                }
                // find message 
                if (user.messages && user.messages.has(senderId))  
                {
                    // delete messages
                    user.messages.delete(senderId)
                }
                // save user
                user.save(function (err) 
                {
                    if (err) 
                    {
                        return res.status(400).json({error:{msg:err ? err.message : 'Internal server error'}});
                    }
                    else 
                    {
                        res.status(200).json({"success":true})
                    }
                })
            }
            else return res.status(400).json({error:{msg:'User hit not found'}});
        }
    })
    
});

module.exports = router;