const mongoose = require('mongoose')
const passport = require('passport')
const router = require('express').Router()
const auth = require(`${process.env.srcDir}auth/Auth`)
const firebase = require(`${process.env.srcDir}firebase/Firebase`)
const Users = mongoose.model('User')

//POST message to user id
router.post('/messageMatch', auth.required, (req, res, next) => 
{
    if (!req.body && !req.body.conversation || !req.body.conversation.user || !req.body.conversation.userMatch || ! req.body.conversation.message) 
    {
        return res.status(400).json({error:{msg:'Request body data missing'}});
    }

    const userBody = req.body.conversation.user;
    const id = userBody.id;

    const userMatchBody = req.body.conversation.userMatch;
    const userMatchId = userMatchBody.id;

    const messageBody = req.body.conversation.message;
    const msg = messageBody.msg.trim();
    const timestamp = Date.now().toString();

    if (id == userMatchId) return res.status(400).json({error:{msg:'User could send message to himself'}});

    Users.findById({_id: id}, function (err, currentUser) 
    {
        if (!currentUser) 
        {
          return res.status(400).json({error:{msg:err ? err.message : 'Internal server error'}});
        }
        else 
        {
            let currentUserName = currentUser.username
            let hitUserName = ""
            Users.findById({_id:userMatchId}, function (err, hitUser) 
            {
                if (!hitUser) {
                    return res.status(400).json({error:{msg:err ? err.message : 'Internal server error'}});
                } else {

                    // check both user has a match or dismatch therefore its not allowed to send messages
                    if (!checkMessageSendAllowed(currentUser, userMatchId) || !checkMessageSendAllowed(hitUser, id)) {
                        return res.status(400).json({error:{msg:"Not allowed to write to user"}});
                    }

                    hitUserName = hitUser.username
                    currentUser.initMessages(userMatchId, timestamp, msg, id, hitUserName, hitUser.accountImages.get("0").accountImageCreatedAt, hitUser.accountImages.get("0").accountImageUpdatedAt, hitUser.updatedAt, true);

                    currentUser.save(function (err){
                        if (err) {
                            return res.status(400).json({error:{msg:'Internal server error'}});
                        } else {
                            const returnUser = currentUser.messages.get(userMatchId)
                            hitUser.initMessages(id, timestamp, msg, id, currentUserName, currentUser.accountImages.get("0").accountImageCreatedAt, currentUser.accountImages.get("0").accountImageUpdatedAt, currentUser.updatedAt, false);
                            hitUser.save(function (err) 
                            {
                                if (err)
                                {
                                    return res.status(400).json({error:{msg:err.message}});
                                }
                                else 
                                {
                                    if (hitUser.deviceToken != "") 
                                    {
                                        var payload = { "notification": {
                                            "title": `New Message from ${currentUser.username}`,
                                            "body": `Message: ${msg}`},
                                            "data": {
                                                "notificationType": "0",
                                                "id":currentUser.id
                                            }
                                        };
                                        var options = {priority: "high", timeToLive: 60 * 60 *24}

                                        firebase.messaging().sendToDevice(hitUser.deviceToken, payload, options)
                                        .then(function(response) {
                                        console.log("Successfully sent message:", response);
                                        })
                                        .catch(function(error) {
                                        console.log("Error sending message:", error);
                                        });
                                    }

                                    res.status(200).json({"conversation": returnUser})
                                }
                            })
                        }
                    })
                }
            })
        }
    })
    
});

function checkMessageSendAllowed(currentUser, userMatchId) {
    let currentUserMatches = currentUser.matches
    let currentUserDisMatches = currentUser.dismatches
    if (!currentUserMatches) {
        return false
    } else {
        if (currentUserMatches.indexOf(userMatchId) == -1) {
            return false
        } else if (currentUserMatches.indexOf(userMatchId) != -1) {
            return true
        }
    }
    if (currentUserDisMatches) {
        if (currentUserDisMatches.indexOf(userMatchId) != -1) return false
    }
    return true
}

//DEL message with user
router.delete('/deleteMessage', auth.required, (req, res, next) => 
{
    if (!req.query && !req.query.id || !req.query.userMatchId) 
    {
        return res.status(400).json({error:{msg:'Request query data missing'}});
    }

    const id = req.query.id
    const userMessageId = req.query.userMatchId

    Users.findById({_id: id}, function(err, user) 
    {
        if (!user) 
        {
            return res.status(400).json({error:{msg:'User not found'}});
        } 
        else 
        {
            if (user.messages && user.messages.has(userMessageId)) 
            {
                user.messages.delete(userMessageId)
                user.save(function (err) {
                    if (err) 
                    {
                        return res.status(400).json({error:{msg:err ? err.message : 'Internal server error'}});
                    }
                    else 
                    {
                        res.status(200).json({"success":true})
                    }
                })
            }
            else 
            {
                return res.status(400).json({error:{msg:'Message to delete not found'}});
            }
        } 
    })

})

//GET message to user id
router.get('/messagesUser', auth.required, (req, res, next) => 
{
    if (!req.query && !req.query.id) 
    {
        return res.status(400).json({error:{msg:'Request query data missing'}});
    }

    const id = req.query.id;

    Users.findById({_id: id}, function (err, user) 
    {
      if (!user) 
      {
        return res.status(400).json({error:{msg:'User not found'}});
      }
      else 
      {
        if (!user.messages) return res.status(200).json({user:{id:id,messages:[]}});
        res.status(200).json(user.toProfileMessages(id))
      }
    })
});

//POST markConversation
router.post('/markConversation', auth.required, (req, res, next) => 
{
    if (!req.body || !req.body.user) 
    {
        return res.status(400).json({error:{msg:'Request body data missing'}});
    }

    const userBody = req.body.user
    const id = userBody.id
    const senderId = userBody.senderId

    Users.findById({_id: id}, function (err, user) 
    {
      if (!user) 
      {
        return res.status(400).json({error:{msg:'User not found'}});
      }
      else 
      {
        if (user.messages && user.messages.get(senderId)) {
            let conversations = user.messages.get(senderId).conversations
            user.messages.get(senderId).conversations = []
            conversations.forEach(con => 
            {   
                con.messageRead = true
                user.messages.get(senderId).conversations.push(con);
            })
            
            user.save(function (err) {
                
                if (err) 
                {
                    return res.status(400).json({error:{msg:err ? err.message : 'Internal server error'}});
                }
                else 
                {
                    res.status(200).json({"conversation": user.messages.get(senderId)})
                }
            })
        }
        else return res.status(400).json({error:{msg:'Conversation to mark not found'}});
      }
    })
    

});

module.exports = router;