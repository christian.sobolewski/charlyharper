const mongoose = require('mongoose');
const passport = require('passport');
const router = require('express').Router();
const auth = require(`${process.env.srcDir}auth/Auth`);
const Users = mongoose.model('User');

//Post updateProfile (required, only authenticated users have access)
router.post('/updateProfile', auth.required, (req, res, next) => 
{
    if (!req.body && !req.body.user && !req.body.user.id) 
    {
        return res.status(400).json({error:{msg:'Request body data missing'}});
    }

    const userBody = req.body.user;
    const id = userBody.id;
    
    const gender = userBody.gender == 0 || userBody.gender == 1 ? userBody.gender : null;
    const birthdate = userBody.birthdate ? userBody.birthdate : null;
    const bio = userBody.bio ? userBody.bio : null;
    const username = userBody.username ? userBody.username : null;

    const update = {}
    if (gender == 0 || gender == 1) update.gender = gender
    if (birthdate) update.birthdate = birthdate
    if (bio) update.bio = bio
    if (username) update.username = username

    Users.findByIdAndUpdate({_id:id}, update, {new: true}, function(err, user) 
    {
      if (err) 
      {
        return res.status(400).json({error:{msg:'User not found'}});
      } 
      else 
      {
        if (!user) 
        {
          return res.status(400).json({error:{msg:'User not found'}});
        } 
        else 
        {
          res.status(200).json({ user: user.toAuthJSON() })
        }
      }
    })
    
});

//Post updateProfile (required, only authenticated users have access)
router.post('/updateFirebaseToken', auth.required, (req, res, next) => 
{
    if (!req.body && !req.body.user && !req.body.user.id && !req.body.user.deviceToken) 
    {
        return res.status(400).json({error:{msg:'Request body data missing'}});
    }

    const userBody = req.body.user;
    const id = userBody.id;
    
    const deviceToken = userBody.deviceToken ? userBody.deviceToken : null
    const update = {}
    if (deviceToken) update.deviceToken = deviceToken

    Users.findById({_id:userBody.id}, function (err, currentUser) 
    {
      if (err) 
      {
        return res.status(400).json({error:{msg:'User not found'}});
      } 
      else 
      {
        if (deviceToken != currentUser.deviceToken) 
        {
          Users.findByIdAndUpdate({_id:id}, update, {new: true}, function(err, user) 
          {
            if (err) 
            {
              return res.status(400).json({error:{msg:'User not found'}});
            } 
            else 
            {
              if (!user) 
              {
                return res.status(400).json({error:{msg:'User not found'}});
              } 
              else 
              {
                res.status(200).json({ success: true })
              }
            }
          })
        } else res.status(200).json({ success: true })
      } 
    })
});

module.exports = router;