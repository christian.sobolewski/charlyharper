// get express module
const express = require('express');
// get express-fileupload
const fileUpload = require('express-fileupload');
// get helmet module
const helmet = require('helmet')
// get body parser
const bodyParser = require('body-parser');
// create session store for mongoDB
const cookieParser = require('cookie-parser');
const session = require('express-session')
const mongoose = require('mongoose');
mongoose.set('debug', true);
const MongoStore = require('connect-mongo')(session);

// app models
require(`${process.env.srcDir}api/user/model/User`);
require(`${process.env.srcDir}api/user/model/Token`);
require(`${process.env.srcDir}api/user/passport/Passport`);

// application router
const loginRouter = require(`${process.env.srcDir}api/user/login/LoginRouter`);
const registerRouter = require(`${process.env.srcDir}api/user/register/RegisterRouter`);
const updateRouter = require(`${process.env.srcDir}api/user/update/UpdateRouter`);
const deleteUserRouter = require(`${process.env.srcDir}api/user/delete/DeleteUserRouter`);
const matchRouter = require(`${process.env.srcDir}api/user/matches/UserMatchesRouter`);
const messagesRouter = require(`${process.env.srcDir}api/user/messages/MessagesRouter`);
const messageSupportRouter = require(`${process.env.srcDir}api/user/messages/MessageSupportRouter`);
const locationUsersRouter = require(`${process.env.srcDir}api/user/location/LocationUsersRouter`);
const locationUserRouter = require(`${process.env.srcDir}api/user/location/LocationUserRouter`);
const accountImageRouter = require(`${process.env.srcDir}api/user/images/AccountImageRouter`);
const forgetPasswordRouter = require(`${process.env.srcDir}api/user/password/ForgotPasswordRouter`);
const filterRouter = require(`${process.env.srcDir}api/user/filter/FilterRouter`);
const paymentRouter = require(`${process.env.srcDir}api/user/payment/PaymentRouter`);
// support router
const supportRouter = require(`${process.env.srcDir}api/user/support/SupportRouter`);

// check if we are on a unit test env
const isUnitTest = process.env.APP_ENV === 'test' ? true : false;

// get http or https based on deployment profile
const http = require(process.env.HTTP);
// create out express instance
const expressApp = express();
// this middleware function will be executed for EVERY request to the app
expressApp.use(function (req, res, next) 
{
  res.removeHeader('x-powered-by');
  // check if mongoose is connected
  if (mongoose.connection.readyState === 1) 
  {
    next();
  } 
  else 
  {
    let mongooseOptions = 
    {
      useNewUrlParser:true,
      useUnifiedTopology:true,
      auth: {"user":process.env.MONGO_USER,"password":process.env.MONGO_PASSWORD},
      dbName:process.env.MONGO_DB,
      useFindAndModify:false  // see https://mongoosejs.com/docs/deprecations.html#findandmodify
    }

    // connect to mongoDB
    mongoose.connect(process.env.MONGO_URL, mongooseOptions).
    catch(error => handleError(error, res)).then(function (args){
      expressApp.use(session({
        secret: 'my-secret',
        resave: false,
        saveUninitialized: true,
        store: new MongoStore({ mongooseConnection: mongoose.connection })
      }));
      next();
    });
  }
})

// configure express app
// default options
expressApp.use(fileUpload({debug:process.env.NODE_ENV !== "production" ? true : false}));
expressApp.use(bodyParser.urlencoded({ extended: false }));
expressApp.use(bodyParser.json());
expressApp.use(cookieParser());
expressApp.use(helmet());
// // Sets "X-XSS-Protection: 1; mode=block". https://helmetjs.github.io/docs/xss-filter/ 
expressApp.use(helmet.xssFilter());
// documentation https://helmetjs.github.io/docs/frameguard/
expressApp.use(helmet.frameguard({action: 'deny'}))
// Sets "X-Download-Options: noopen". documentation https://helmetjs.github.io/docs/ienoopen/
expressApp.use(helmet.ieNoOpen());
// documentation https://helmetjs.github.io/docs/nocache/
expressApp.use(helmet.noCache());
// Sets "X-Content-Type-Options: nosniff". documenation https://helmetjs.github.io/docs/dont-sniff-mimetype/
expressApp.use(helmet.noSniff());
// Sets "Referrer-Policy: same-origin". documentation https://helmetjs.github.io/docs/referrer-policy/
expressApp.use(helmet.referrerPolicy({policy: 'same-origin'}));

// API Use
expressApp.use('/api/user/login', loginRouter);
expressApp.use('/api/user/register', registerRouter);
expressApp.use('/api/user/update', updateRouter);
expressApp.use('/api/user/delete', deleteUserRouter);
expressApp.use('/api/user/matches', matchRouter);
expressApp.use('/api/user/messages', messagesRouter);
expressApp.use('/api/user/messagesSupport', messageSupportRouter);
expressApp.use('/api/user/location', locationUsersRouter);
expressApp.use('/api/user/location', locationUserRouter);
expressApp.use('/api/user/images', accountImageRouter);
expressApp.use('/api/user/password', forgetPasswordRouter);
expressApp.use('/api/user/filter', filterRouter);
expressApp.use('/api/user/payment', paymentRouter);
expressApp.use('/api/user/support', supportRouter);

module.exports = start = async (unitTest) => 
{
  try 
  {
    const server = http.createServer({}, expressApp).listen(
      process.env.PORT, 
      process.env.HOST, 
      0, 
      function() 
      {
        let serverEnv = `${process.env.HTTP}://${process.env.HOST}:${process.env.PORT}`
        let serverInfo = '';
        // test mode calls callback unitTest
        if (isUnitTest) 
        {
          unitTest();
          serverInfo = `Rest API available on: ${serverEnv}/api`;
        } 
        else 
        {
          serverInfo = `Rest API available on: ${serverEnv}/api\nStatus available on: ${serverEnv}/status\nSwagger Docs available on: ${serverEnv}/api-docs`;
        }
        console.log(serverInfo);
      }
    );
    //if (!isUnitTest) { io.attach(server); } else unitTest();

  } 
  catch (e) 
  {
    // test mode calls callback unitTest
    if (isUnitTest) unitTest(e); else console.log(e);
  }
};
  
function handleError(error, res) 
{
  res.status(500).json(error);
}