var AWS = require("aws-sdk");
AWS.config.secretAccessKey= process.env.AWS_SECRET_ACCESS_KEY;
AWS.config.accessKeyId = process.env.AWS_ACCESS_KEY_ID;
AWS.config.region = process.env.AWS_REGION;
AWS.config.apiVersions = '2006-03-1';

AWS.config.getCredentials
(
    function(err) 
    {
        if (err) console.log(err.stack);
        // credentials not loaded
    else 
    {
        console.log("Access key:", AWS.config.credentials.accessKeyId);
        console.log("Secret access key:", AWS.config.credentials.secretAccessKey);
    }
});

// Create a S3 service object
var s3 = new AWS.S3();

// Call S3 to list the buckets
s3.listBuckets
(
    function(err, data) 
    {
        if (err) 
        {
            console.log("Error", err);
        } 
        else 
        {
            console.log("Success", JSON.stringify(data.Buckets).toString());
        }
    }
);

module.exports = s3;