var firebaseAdmin = require("firebase-admin");

var serviceAccount = require("./bambam-80ab8-firebase-adminsdk-sel7w-92a519af25.json");

firebaseAdmin.initializeApp({
  credential: firebaseAdmin.credential.cert(serviceAccount),
  databaseURL: "https://bambam-80ab8.firebaseio.com"
});

/*
firebaseAdmin.messaging().sendToDevice(registrationToken, payload, options)
  .then(function(response) {
    console.log("Successfully sent message:", response);
  })
  .catch(function(error) {
    console.log("Error sending message:", error);
  });
  */

module.exports = firebaseAdmin;