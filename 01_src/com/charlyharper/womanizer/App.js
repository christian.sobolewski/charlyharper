console.log('com.charlyharper.womanizer');

if (process.env.NODE_ENV === 'production' && process.env.CLUSTER_ENV !== "AWS") 
{
    // Info: on production mode and on AWS the env will be set by kubernetes
    //require('custom-env').env('production', 'usr/01_src/com/charlyharper/womanizer/env');
}
else if (process.env.NODE_ENV === 'mobile')
{
    // this will load .env file which is default dev mode
    require('custom-env').env('mobile', '01_src/com/charlyharper/womanizer/env');
}
else 
{
    if (process.env.NODE_ENV === 'production' && process.env.CLUSTER_ENV !== "AWS") 
    {
        require('custom-env').env('development', 'com/charlyharper/womanizer/env');
        process.env.HOST= '0.0.0.0'
        process.env.PORT= 8000 
    }
    else  if (process.env.CLUSTER_ENV !== "AWS")
    {
        // this will load .env file which is default dev mode
        require('custom-env').env('development', '01_src/com/charlyharper/womanizer/env');
    }
}

//get app directory and set it to nodejs process.env.scrDir  
process.env.srcDir = `${process.cwd()}${process.env.PACKAGE_NAME}`
if (process.env.NODE_ENV === 'production' && process.env.CLUSTER_ENV !== 'AWS')
{
    process.env.srcDir = `${process.cwd()}/com/charlyharper/womanizer/`
}

console.log(`com.charlyharper.womanizer:: NODE_ENV ${process.env.NODE_ENV}`);
console.log(`com.charlyharper.womanizer:: CLUSTER_ENV  ${process.env.CLUSTER_ENV}`);
console.log(`com.charlyharper.womanizer:: HOST  ${process.env.HOST}`);
console.log(`com.charlyharper.womanizer:: PORT  ${process.env.PORT}`);
console.log(`com.charlyharper.womanizer:: PACKAGE_NAME ${process.env.PACKAGE_NAME}`);
console.log(`com.charlyharper.womanizer:: AWS_ACCESS_KEY_ID ${process.env.AWS_ACCESS_KEY_ID}`);
console.log(`com.charlyharper.womanizer:: AWS_SECRET_ACCESS_KEY ${process.env.AWS_SECRET_ACCESS_KEY}`);
console.log(`com.charlyharper.womanizer:: AWS_REGION ${process.env.AWS_REGION}`);
console.log(`com.charlyharper.womanizer:: AWS_BUCKET ${process.env.AWS_BUCKET}`);
console.log(`com.charlyharper.womanizer:: MONGO_URL ${process.env.MONGO_URL}`);
console.log(`com.charlyharper.womanizer:: MONGO_DB ${process.env.MONGO_DB}`);
console.log(`com.charlyharper.womanizer:: MONGO_USER ${process.env.MONGO_USER}`);
console.log(`com.charlyharper.womanizer:: MONGO_PASSWORD ${process.env.MONGO_PASSWORD}`);
console.log(`com.charlyharper.womanizer:: ${process.cwd()}`);
console.log(`com.charlyharper.womanizer:: ${process.env.srcDir}`);

//get server module
const server = require(`${process.env.srcDir}server/Server`);
//Main entry point
server();

//get firebase module
const firebase = require(`${process.env.srcDir}firebase/Firebase`);

console.log('com.charlyharper.womanizer initialized');